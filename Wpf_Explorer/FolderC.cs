﻿using System;
using System.IO;
using System.Windows.Data;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections;

namespace Wpf_Explorer
{
    public class FolderC : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        private long _fileCount;
        private long _folderCount;
        private string _title;
        private bool _isChecked;
        private long _size;

        public static long totalFiles = 0;
        public static long totalSize = 0;

        public long FileCount
        {
            get { return _fileCount; }
            set { _fileCount = value; if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("FileCount")); }
        }

        public long FolderCount
        {
            get { return _folderCount; }
            set { _folderCount = value; if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("FolderCount")); }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Title")); }
        }

        public bool IsChecked
        {
            get { return _isChecked; }
            set { _isChecked = value; if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("IsChecked")); }
        }

        public long Size
        {
            get { return _size; }
            set { _size = value; if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Size")); }
        }

        public ObservableCollection<FolderC> Folders { get; set; }
        public ObservableCollection<FileC> Files { get; set; }

        public FolderC()
        {
            this.Folders = new ObservableCollection<FolderC>();
            this.Files = new ObservableCollection<FileC>();
        }

        //public class FolderMultiValueConverter : IMultiValueConverter
        //{
        //    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        //    {
        //        string fileCount = values[0].ToString();
        //        string folderCount = values[1].ToString();
        //        string title = values[2].ToString();
        //        string isChecked = values[3].ToString();
        //        string size = values[4].ToString();

        //        return String.Format("File count: {0}, Folder Count: {1}, Title: {2}, IsChecked: {3}, Size: {4}", fileCount, folderCount, title, isChecked, size);
        //    }

        //    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        //    {
        //        throw new NotImplementedException();
        //    }
        //}

        public IList Children
        {
            get
            {
                return new CompositeCollection()
                {
                    new CollectionContainer() {Collection = Folders},
                    new CollectionContainer() {Collection = Files}
                };
            }
        }

        public static void DirSearch(string sDir, ref FolderC root)
        {
            try
            {

                foreach (string f in Directory.GetFiles(sDir))
                {
                    FileC file = new FileC() { Title = System.IO.Path.GetFileName(f), Size = new FileInfo(f).Length, Parent = root };
                    root.Files.Add(file);
                    root.Size += new FileInfo(f).Length;
                    root.FileCount++;
                    FolderC.totalFiles++;
                    FolderC.totalSize += new FileInfo(f).Length;
                }

                foreach (string d in Directory.GetDirectories(sDir))
                {
                    root.FolderCount++;

                    FolderC folder = new FolderC() { Title = new DirectoryInfo(d).Name };
                    long folderSize = 0;
                    DirSearch(d, ref folder);

                    folder.Size = folderSize;
                    root.Folders.Add(folder);
                }
            }
            catch (System.Exception excpt)
            {
                Console.WriteLine(excpt.Message);
            }
        }

        public FolderC getFullDirectory(string dir)
        {
            FolderC root = new FolderC() { Title = new DirectoryInfo(dir).Name };
            FolderC.DirSearch(dir, ref root);
            return root;
        }
    }
}
