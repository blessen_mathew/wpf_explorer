﻿using System.ComponentModel;

namespace Wpf_Explorer
{
    public class FileC : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string _title;
        private long _size;
        private bool _isChecked;
        private FolderC _parent;

        public string Title
        {
            get { return _title; }
            set { _title = value; if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Title")); }
        }
        public long Size
        {
            get { return _size; }
            set { _size = value; if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("Size")); }
        }
        public bool IsChecked
        {
            get { return _isChecked; }
            set { _isChecked = value; if (PropertyChanged != null) PropertyChanged(this, new PropertyChangedEventArgs("IsChecked")); }
        }

        public FolderC Parent
        {
            get { return _parent; }
            set { _parent = value; }
        }
    }
}
