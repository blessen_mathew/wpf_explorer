﻿using System.Windows.Controls;

namespace Wpf_Explorer
{
    public class MyCheckBox : CheckBox
    {
        public MyCheckBox()
        {
            base.IsChecked = false;
        }

        protected override void OnToggle()
        {
            if (this.IsChecked == true)
                this.IsChecked = false;
            else
                this.IsChecked = true;
        }
    }
}
