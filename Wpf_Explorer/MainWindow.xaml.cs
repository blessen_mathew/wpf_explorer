﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Runtime.Remoting.Messaging;
using System.Windows.Threading;
using System.Collections;

namespace Wpf_Explorer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            this.Loaded += Window_Loaded;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            FolderC root = new FolderC() { Title = "Root", IsChecked = false };
            FolderC.DirSearch(".", ref root);
            tv.Items.Add(root);
            //lblTotalFiles.Content = root.FileCount.ToString();
            lblTotalFiles.Content = FolderC.totalFiles;
            lblTotalSize.Content = FolderC.totalSize;
        }


        private delegate FolderC MyFolderFunctionDelegate(string dir);

        private void button_Click(object sender, RoutedEventArgs e)
        {

            try
            {
                tv.Items.Clear();
                SelectedFiles = 0;
                TotalFiles = 0;
                FolderC.totalFiles = 0;
                FolderC.totalSize = 0;
                var dialog = new System.Windows.Forms.FolderBrowserDialog();
                System.Windows.Forms.DialogResult result = dialog.ShowDialog();
                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    lblDir.Content = "Result: " + dialog.SelectedPath;
                }

                FolderC theFolder = new FolderC();
                MyFolderFunctionDelegate theFolderDelegate = new MyFolderFunctionDelegate(theFolder.getFullDirectory);
                lblMessage.Content = "Begin parsing - " + dialog.SelectedPath;
                theFolderDelegate.BeginInvoke(dialog.SelectedPath, theCallback, this);

                //FolderC root = new Wpf_Explorer.FolderC() { Title = new DirectoryInfo(dialog.SelectedPath).Name };

                //FolderC root = getFullDirectory(dialog.SelectedPath);

                //DirSearch(dialog.SelectedPath, ref root);

                //tv.Items.Add(root);
                //lblTotalFiles.Content = root.FileCount.ToString();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void theCallback(IAsyncResult theResults)
        {
            AsyncResult result = (AsyncResult)theResults;
            MyFolderFunctionDelegate folderDel = (MyFolderFunctionDelegate)result.AsyncDelegate;
            FolderC answer = folderDel.EndInvoke(theResults);

            this.Dispatcher.Invoke(DispatcherPriority.Background, ((Action)(() => 
            {
                tv.Items.Add(answer);
                //lblTotalFiles.Content = answer.FileCount.ToString();
                lblTotalFiles.Content = FolderC.totalFiles;
                lblTotalSize.Content = FolderC.totalSize;
                lblMessage.Content = "Done parsing files";
            })));
        }

        private void setSelectedFiles(FolderC folder, bool isChecked)
        {
            foreach (FileC file in folder.Files)
            {
                file.IsChecked = isChecked;

                //if (isChecked == true)
                //    SelectedFiles++;
                //else
                //    SelectedFiles--;
            }

            foreach (FolderC f in folder.Folders)
            {
                setSelectedFiles(f, isChecked);
                f.IsChecked = isChecked;
                //if (isChecked == true)
                //    SelectedFiles += f.FileCount;
                //else
                //    SelectedFiles -= f.FileCount;
            }

            //folder.IsChecked = isChecked;

            //if (isChecked == true)
            //    SelectedFiles += folder.FileCount;
            //else
            //    SelectedFiles -= folder.FileCount;

            //if (isChecked == true)
            //    SelectedFiles += folder.FileCount;
            //else
            //    SelectedFiles -= folder.FileCount;
        }

        //Move the logic to external method
        private void chk_Checked(object sender, RoutedEventArgs e)
        {
            //for each file child you take the size and add to the number
            //for each folder you take each file - for each folder you go in
            //recursion
            var selectedFolder = (sender as CheckBox).DataContext as FolderC;
            setSelectedFiles(selectedFolder, true);
            //store long here of total size that is determined in setSelectedFiles
            //add to the dependency property
            lblSelectedFiles.Content = SelectedFiles.ToString();

        }

        private void chk_Unchecked(object sender, RoutedEventArgs e)
        {
            var selectedFolder = (sender as CheckBox).DataContext as FolderC;
            setSelectedFiles(selectedFolder, false);

            lblSelectedFiles.Content = SelectedFiles.ToString();
        }

        private void chk_Checked_1(object sender, RoutedEventArgs e)
        {
            var selectedFile = (sender as CheckBox).DataContext as FileC;
            selectedFile.IsChecked = true;
            SelectedFiles++;
        }

        private void chk_Unchecked_1(object sender, RoutedEventArgs e)
        {
            var selectedFile = (sender as CheckBox).DataContext as FileC;
            selectedFile.IsChecked = false;
            SelectedFiles--;
        }


        public long SelectedFiles
        {
            get { return (long)GetValue(SelectedFilesProperty); }
            set { SetValue(SelectedFilesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for SelectedFiles.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SelectedFilesProperty =
            DependencyProperty.Register("SelectedFiles", typeof(long), typeof(MainWindow), new PropertyMetadata((long)0));

         
        public long TotalFiles
        {
            get { return (long)GetValue(TotalFilesProperty); }
            set { SetValue(TotalFilesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for TotalFiles.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TotalFilesProperty =
            DependencyProperty.Register("TotalFiles", typeof(long), typeof(MainWindow), new PropertyMetadata((long)0));


    }


}
